FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache bash
# Install dependencies required for psycopg2 python package
RUN apk update && apk add libpq
RUN apk update && apk add --virtual .build-deps gcc python3-dev musl-dev postgresql-dev

WORKDIR /usr/src/app
COPY . .
RUN chmod +x ./wait-for-it.sh
RUN chmod +x ./entrypoint.sh

RUN pip install -r requirements.txt

# Remove dependencies only required for psycopg2 build
RUN apk del .build-deps

EXPOSE 8000

ENTRYPOINT [ "./entrypoint.sh" ]