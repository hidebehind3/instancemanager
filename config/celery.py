import os

from celery import Celery


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

from django.conf import settings  # noqa


app = Celery("InstanceManager")
app.config_from_object(settings, namespace="CELERY")
app.autodiscover_tasks()


@app.register_task
@app.task(bind=True)
def debug_task(self):
    """For debugging purposes"""

    print("Request: {0!r}".format(self.request))
    return repr(self.request)
