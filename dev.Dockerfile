FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache bash
RUN apk update && apk add libpq
RUN apk update && apk add --virtual .build-deps gcc python3-dev musl-dev postgresql-dev

ADD requirements.txt /usr/src/app/requirements.txt
ADD wait-for-it.sh /usr/src/app/wait-for-it.sh

WORKDIR /usr/src/app/

RUN chmod +x ./wait-for-it.sh

RUN pip install -r requirements.txt

ENTRYPOINT [ "./entrypoint.sh" ]