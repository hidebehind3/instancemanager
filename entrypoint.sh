#!/usr/bin/env bash
set -ex

DJANGO_SETTINGS_MODULE="${DJANGO_SETTINGS_MODULE:-config.settings.local}"


migrate(){
    python3 manage.py migrate --no-input
}

collectstatic(){
    python3 manage.py collectstatic --clear --no-input
}


case "$@" in
    start)
        collectstatic
        migrate
        gunicorn \
        -e DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE} \
        -b :${PORTNUM:-8000} --access-logfile - --error-logfile - \
        --name=InstanceManager --workers ${WORKERNUM:-2} --threads ${THREADNUM:-4} \
        config.wsgi:application
    ;;
    start-dev)
        ./wait-for-it.sh postgres:5432
        migrate
        python3 manage.py runserver 0.0.0.0:${PORTNUM:-8000}
    ;;
    test)
        python3 manage.py check
        python3 manage.py test --no-input
    ;;
    worker)
        celery -A config worker -l info --max-tasks-per-child 7 --concurrency=2 --task-events
    ;;
    scheduler)
        rm -f celerybeat.pid || true
        celery -A config beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
    ;;
    *)
        $@
    ;;
esac
