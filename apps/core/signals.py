from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db import transaction

from .models import CompanyInstance
from .tasks import provision_company_instance


@receiver(post_save, sender=CompanyInstance)
def company_instance_post_save(instance, created, **kwargs):
    if created:
        transaction.on_commit(lambda: provision_company_instance.delay(instance.pk))
