from celery import shared_task

from .models import CompanyInstance


@shared_task
def provision_company_instance(company_instance_id):
    company_instance = CompanyInstance.objects.get(pk=company_instance_id)
    company_instance.status = CompanyInstance.CREATING
    company_instance.save(update_fields=["status"])
