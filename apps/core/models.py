from django.db import models


class PRAgency(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name_plural = "PR Agencies"

    def __str__(self):
        return self.name


class CompanyInstance(models.Model):
    NEW = "NEW"
    CREATING = "CREATING"
    PROVISIONED = "PROVISIONED"
    STATUS_CHOICES = (
        (NEW, "NEW"),
        (CREATING, "CREATING"),
        (PROVISIONED, "PROVISIONED"),
    )

    name = models.CharField(max_length=255, unique=True)
    pr_agencies = models.ManyToManyField(PRAgency)
    url = models.URLField()
    status = models.CharField(choices=STATUS_CHOICES, default=NEW, max_length=20)

    database_health = models.BooleanField(default=False)
    cache_health = models.BooleanField(default=False)
    http_health = models.BooleanField(default=False)

    starttime = models.CharField(max_length=120, blank=True)
    uptime = models.CharField(max_length=120, blank=True)

    last_checked_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def health(self):
        return self.database_health and self.cache_health and self.http_health
