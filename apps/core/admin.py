from django.contrib import admin

from .models import PRAgency, CompanyInstance


@admin.register(PRAgency)
class PRAgencyAdmin(admin.ModelAdmin):
    pass


@admin.register(CompanyInstance)
class CompanyInstanceAdmin(admin.ModelAdmin):
    list_display = ("__str__", "status", "health")
    exclude = ("status",)
    readonly_fields = (
        "database_health",
        "cache_health",
        "http_health",
        "starttime",
        "uptime",
        "last_checked_at",
    )

    def health(self, instance):
        return instance.health

    health.boolean = True
