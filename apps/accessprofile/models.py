from access.managers import AccessManager
from access.plugins import CompoundPlugin, CheckAblePlugin, ApplyAblePlugin

from apps.custom_auth.models import CustomUser
from apps.core.models import PRAgency, CompanyInstance


def perm_role(roles, request):
    return request.user.is_superuser or getattr(request.user, "role", None) in roles


PERMISSIONS = {
    PRAgency: CompoundPlugin(
        CheckAblePlugin(
            appendable=lambda model, request: (
                {} if perm_role((CustomUser.NDT, CustomUser.NDE), request) else False
            )
        ),
        ApplyAblePlugin(
            visible=lambda queryset, request: (
                queryset.all()
                if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                else queryset.filter(pk=request.user.pr_agency.pk)
            ),
            changeable=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(pk=request.user.pr_agency.pk)
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.none()
            ),
            deleteable=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(pk=request.user.pr_agency.pk)
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.none()
            ),
        ),
    ),
    CompanyInstance: CompoundPlugin(
        CheckAblePlugin(
            appendable=lambda model, request: (
                {}
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else False
            )
        ),
        ApplyAblePlugin(
            visible=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(
                        pr_agencies__in=[getattr(request.user, "pr_agency", [])]
                    )
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.none()
            ),
            changeable=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(
                        pr_agencies__in=[getattr(request.user, "pr_agency", [])]
                    )
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.none()
            ),
            deleteable=lambda queryset, request: (
                queryset.all()
                if perm_role((CustomUser.NDT,), request)
                else queryset.none()
            ),
        ),
    ),
    CustomUser: CompoundPlugin(
        CheckAblePlugin(appendable=lambda model, request: {}),
        ApplyAblePlugin(
            visible=lambda queryset, request: (
                queryset.all()
                if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                else queryset.filter(pr_agency=getattr(request.user, "pr_agency", None))
            ),
            changeable=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(
                        pr_agency=getattr(request.user, "pr_agency", [])
                    )
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.filter(pk=request.user.pk)
            ),
            deleteable=lambda queryset, request: (
                (
                    queryset.all()
                    if perm_role((CustomUser.NDT, CustomUser.NDE), request)
                    else queryset.filter(
                        pr_agency=getattr(request.user, "pr_agency", [])
                    )
                )
                if perm_role((CustomUser.NDT, CustomUser.NDE, CustomUser.PRA), request)
                else queryset.none()
            ),
        ),
    ),
}


AccessManager.register_plugins(PERMISSIONS)
