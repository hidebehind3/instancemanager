from django.apps import AppConfig


class AccessprofileConfig(AppConfig):
    name = "apps.accessprofile"
