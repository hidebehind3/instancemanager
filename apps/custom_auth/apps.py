from django.apps import AppConfig


class CustomAuthConfig(AppConfig):
    name = "apps.custom_auth"

    def ready(self):
        import apps.custom_auth.signals
