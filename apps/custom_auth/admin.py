from django.contrib import admin
from django.utils.crypto import get_random_string
from django.contrib.auth.forms import PasswordResetForm

from authtools.admin import StrippedUserAdmin

from .models import CustomUser
from .forms import CustomUserCreationForm


@admin.register(CustomUser)
class CustomUserAdmin(StrippedUserAdmin):
    add_form = CustomUserCreationForm

    add_fieldsets = (
        (
            None,
            {
                "description": (
                    "Enter the new user's name and email address and click save."
                    " The user will be emailed a link allowing them to login to"
                    " the site and set their password."
                ),
                "fields": ("email", "name", "role"),
            },
        ),
        (
            "Password",
            {
                "description": "Optionally, you may set the user's password here.",
                "fields": ("password1", "password2"),
                "classes": ("collapse", "collapse-closed"),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        obj.is_staff = True

        if obj.role == CustomUser.NDT:
            obj.is_superuser = True

        if not change:

            if not obj.has_usable_password():
                # Django's PasswordResetForm won't let us reset an unusable
                # password. We set it above super() so we don't have to save twice.
                obj.set_password(get_random_string())

            super().save_model(request, obj, form, change)

            reset_form = PasswordResetForm({"email": obj.email})
            assert reset_form.is_valid()
            reset_form.save(
                subject_template_name="custom_auth/emails/account_creation_subject.txt",
                email_template_name="custom_auth/emails/account_creation_email.html",
            )
