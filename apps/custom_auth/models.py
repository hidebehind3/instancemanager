from django.db import models

from authtools.models import AbstractNamedUser


class CustomUser(AbstractNamedUser):
    NDT = "NDT"
    NDE = "NDE"
    PRA = "PRA"
    PRU = "PRU"
    ROLES_CHOICES = (
        (NDT, "News Direct Tech/Admin"),
        (NDE, "News Direct Employee/Sales"),
        (PRA, "PR Agency Admin"),
        (PRU, "PR Agency User"),
    )

    role = models.CharField(max_length=3, choices=ROLES_CHOICES, default=NDT)
    pr_agency = models.ForeignKey(
        "core.PRAgency", blank=True, null=True, on_delete=models.SET_NULL
    )

    class Meta(AbstractNamedUser.Meta):
        swappable = "AUTH_USER_MODEL"

    @property
    def permissions(self):
        return {
            self.NDT: {"create_user_type": [self.NDT, self.NDE, self.PRA, self.PRU]},
            self.NDE: {"create_user_type": [self.NDE, self.PRA, self.PRU]},
            self.PRA: {"create_user_type": [self.PRA, self.PRU]},
            self.PRU: {"create_user_type": [self.PRU]},
        }.get(self.role, {})
