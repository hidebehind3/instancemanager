#! /usr/bin/make -f


help:
	@echo "Makefile for InstanceManager"
	@echo
	@echo 'Usage:'
	@echo '    make run-dev   - run project in dev mode within docker container'
	@echo '    make run       - run project in docker container'
	@echo '    make test      - run tests in project'
	@echo
	@echo 'Code style (best to run it before every commit):'
	@echo '    make black     - run black formatter'
	@echo '    make flake8    - run flake8 linter'
	@echo '    make code-analysis    - run black and flake8'


run-dev:
	docker-compose up -d --build --force-recreate --remove-orphans


run:
	docker-compose -f docker-compose.yml up -d --build --force-recreate --remove-orphans


test:
	docker-compose run --name backend backend test


black:
	black apps/ config/


flake8:
	flake8 apps/ config/ --ignore W503 --per-file-ignores config/settings/*:F401,F405


code-analysis: black flake8
